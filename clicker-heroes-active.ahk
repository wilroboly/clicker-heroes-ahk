cooldown = 0 ;your ancient Vaagur cooldown percentage here
level_active_heroes = false ;true to autolevel uncollapsed heroes

WinGetTitle, wintitle, ahk_class ApolloRuntimeContentWindow
WinWait, %wintitle%,
IfWinNotActive, %wintitle%, , WinActivate, %wintitle%,
WinWaitActive, %wintitle%,

#NoEnv
#KeyHistory 0
ListLines Off
SetBatchLines, -1
SetMouseDelay, -1
SetKeyDelay, 20
SetDefaultMouseSpeed, 0
SetWinDelay, -1
Thread, interrupt, 0
SendMode Input

ResizeWin(1152,679)
cd_timer = 6
gamename = Clicker Heroes
wait_timer := ceil(6004 * (100 - cooldown))
SetTimer, Abilities, %wait_timer%

IfInString, gamename, %wintitle%
{
  loop {
    if (%level_active_heroes%) {
      level_active_heroes()
    }
    unlock_prog_mode()
    loop, 20 {
      bee()
      loop, 6 {
        loop, 214 {
          ControlClick, x870 y510, %wintitle%,,,, NA
          Sleep, 16
        }
        ControlClick, x532 y483, %wintitle%,,,, NA
        ControlClick, x747 y434, %wintitle%,,,, NA
        ControlClick, x1008 y454, %wintitle%,,,, NA
        ControlClick, x757 y379, %wintitle%,,,, NA
        ControlClick, x1052 y447, %wintitle%,,,, NA
      }
    }
  }
} else {
  MsgBox, %gamename% is not running. Exiting...
  ExitApp
}

Abilities:
if (cd_timer > 5) {
  ControlSend,, {1 down}{1 up}, %wintitle%
  ControlSend,, {2 down}{2 up}, %wintitle%
  ControlSend,, {3 down}{3 up}, %wintitle%
  ControlSend,, {4 down}{4 up}, %wintitle%
  ControlSend,, {5 down}{5 up}, %wintitle%
  ControlSend,, {7 down}{7 up}, %wintitle%
  ControlSend,, {8 down}{8 up}, %wintitle%
  ControlSend,, {6 down}{6 up}, %wintitle%
  ControlSend,, {9 down}{9 up}, %wintitle%
  cd_timer = -5
  return
}

if (cd_timer < 0) {
  cd_timer++
  return
}

if (cd_timer = 0) {
  ControlSend,, {8 down}{8 up}, %wintitle%
  ControlSend,, {9 down}{9 up}, %wintitle%
  ControlSend,, {1 down}{1 up}, %wintitle%
  ControlSend,, {2 down}{2 up}, %wintitle%
  ControlSend,, {3 down}{3 up}, %wintitle%
  ControlSend,, {4 down}{4 up}, %wintitle%
  ControlSend,, {5 down}{5 up}, %wintitle%
  ControlSend,, {7 down}{7 up}, %wintitle%
  cd_timer++
  return
}

if (cd_timer = 3) {
  ControlSend,, {3 down}{3 up}, %wintitle%
  ControlSend,, {4 down}{4 up}, %wintitle%
}

if (cd_timer > 0) {
  ControlSend,, {1 down}{1 up}, %wintitle%
  ControlSend,, {2 down}{2 up}, %wintitle%
  cd_timer++
  return
} else {
  cd_timer = 6
  return
}

bee() {
  PixelSearch, FoundBeeX, FoundBeeY, 710, 113, 1050, 113, 0x873921, 0, Fast RGB
  if (ErrorLevel = 0) {
    FoundBeeY += 55
    loop, 10 {
      loop, 5 {
        ControlClick, % "x" FoundBeeX " y" FoundBeeY, %wintitle%,,,, NA
        Sleep, 40
      }
      FoundBeeX--
    }
  } else {
  }
}

level_active_heroes() {
  ScrollBarX = 555
  ScrollBarY = 276
  loop, 6 {
    ControlClick, % "x" ScrollBarX " y" ScrollBarY, %wintitle%,,,, NA
    ControlClick, x870 y510, %wintitle%,,,, NA
    Sleep, 350
    PixelSearch, LevelableX, LevelableY, 43, 204, 73, 669, 0xFFF90A, 0, Fast RGB
    toggle := true
    while (toggle) {
      if (ErrorLevel = 0) {
        Send {Q down}
        Sleep, 50
        ControlClick, % "x" LevelableX " y" LevelableY, %wintitle%,,,, NA
        Send {Q up}
        ControlClick, x870 y510, %wintitle%,,,, NA
        Sleep, 740
        PixelSearch, LevelableX, LevelableY, 43, 204, 73, 669, 0xFFF90A, 0, Fast RGB
        Sleep, 450
      } else {
        ScrollBarY += 61
        toggle := false
      }
    }
  }
}

ResizeWin(Width,Height) {
  WinGetPos,X,Y,W,H,%wintitle%
  if (W != %Width% && H != %Height%) {
    WinMove,%wintitle%,,%X%,%Y%,%Width%,%Height%
  }
}

unlock_prog_mode() {
  PixelGetColor, color, 1121, 279, RGB
  Sleep, 50
  if (color = 0xFF0000) {
    ControlClick, % "x" 1121 " y" 279, %wintitle%,,,, NA
  }
}

~!u::
  ControlClick, x555 y640, %wintitle%,,,, NA
  Sleep, 450
  ControlClick, x450 y580, %wintitle%,,,, NA
  Sleep, 100
  ControlClick, x555 y600, %wintitle%,,,, NA
  return

F7::
  ControlClick, x462 y130, %wintitle%,,,, NA
  Sleep, 350
  ControlClick, x146 y420, %wintitle%,,,, NA
  Sleep, 250
  PixelGetColor, color, 198, 683, RGB
  Sleep, 50
  if (color != 0xFFFEFE && color != 0xFFFFFF) {
    ControlClick, x280 y600, %wintitle%,,,, NA
    loop, 640 {
      ControlClick, x870 y510, %wintitle%,,,, NA
      Sleep, 16
    }
    Sleep, 3500
  }
  ControlClick, x50 y130, %wintitle%,,,, NA
  Sleep, 150
  ControlClick, x555 y600, %wintitle%,,,, NA
  return

F8::
  MouseGetPos, xpos, ypos
  Send {Z down}
  Sleep, 50
  ControlClick, % "x" xpos " y" ypos, %wintitle%,,,, NA
  Send {Z up}
  return

F9::
  ControlClick, % "x" 1121 " y" 279, %wintitle%,,,, NA
  return

F11::Pause
F12::ExitApp
