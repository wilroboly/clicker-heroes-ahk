cooldown = 0 ;your ancient Vaagur cooldown percentage here
level_active_heroes = false ;true to autolevel uncollapsed heroes

WinGetTitle, wintitle, ahk_class ApolloRuntimeContentWindow
WinWait, %wintitle%,
IfWinNotActive, %wintitle%, , WinActivate, %wintitle%,
WinWaitActive, %wintitle%,

#NoEnv
#KeyHistory 0
ListLines Off
SetBatchLines, -1
SetMouseDelay, -1
SetKeyDelay, 20
SetDefaultMouseSpeed, 0
SetWinDelay, -1
Thread, interrupt, 0
SendMode Input

ResizeWin(1152,679)
gamename = Clicker Heroes
wait_timer := ceil(36003 * (100 - cooldown))
ddrcastyet := false
SetTimer, InitialDDR, 100
SetTimer, Abilities, %wait_timer%

IfInString, gamename, %wintitle%
{
  loop {
    if (%level_active_heroes%) {
      level_active_heroes()
    }
    unlock_prog_mode()
    loop, 30 {
      bee()
      scan_for_fish(741, 366, 0xBCCC23) ; Pos left middle
      scan_for_fish(507, 473, 0xBCCC23) ; Pos scrollbar left
      scan_for_fish(867, 530, 0xEA5112) ; Pos middle
      scan_for_fish(986, 438, 0xBCCC23) ; Pos right middle
      scan_for_fish(728, 417, 0xBCCC23) ; Pos left
      scan_for_fish(1034, 428, 0xBCCC23) ; Pos right
      Sleep, 16000
    }
  }
} else {
  MsgBox, %gamename% is not running. Exiting...
  ExitApp
}

Abilities:
  if (ddrcastyet = true) {
    ControlSend,, {8 down}{8 up}, %wintitle%
    ControlSend,, {9 down}{9 up}, %wintitle%
    ControlSend,, {1 down}{1 up}, %wintitle%
    ControlSend,, {2 down}{2 up}, %wintitle%
    ControlSend,, {3 down}{3 up}, %wintitle%
    ControlSend,, {4 down}{4 up}, %wintitle%
    ControlSend,, {5 down}{5 up}, %wintitle%
    ControlSend,, {7 down}{7 up}, %wintitle%
    ddrcastyet := false
    return
  } else {
    ControlSend,, {1 down}{1 up}, %wintitle%
    ControlSend,, {2 down}{2 up}, %wintitle%
    ControlSend,, {3 down}{3 up}, %wintitle%
    ControlSend,, {4 down}{4 up}, %wintitle%
    ControlSend,, {5 down}{5 up}, %wintitle%
    ControlSend,, {7 down}{7 up}, %wintitle%
    ControlSend,, {8 down}{8 up}, %wintitle%
    ControlSend,, {6 down}{6 up}, %wintitle%
    ControlSend,, {9 down}{9 up}, %wintitle%
    ddrcastyet := true
    return
  }

InitialDDR:
  ControlSend,, {1 down}{1 up}, %wintitle%
  ControlSend,, {2 down}{2 up}, %wintitle%
  ControlSend,, {3 down}{3 up}, %wintitle%
  ControlSend,, {4 down}{4 up}, %wintitle%
  ControlSend,, {5 down}{5 up}, %wintitle%
  ControlSend,, {7 down}{7 up}, %wintitle%
  ControlSend,, {8 down}{8 up}, %wintitle%
  ControlSend,, {6 down}{6 up}, %wintitle%
  ControlSend,, {9 down}{9 up}, %wintitle%
  ddrcastyet = true
  SetTimer, InitialDDR, off
  return

bee() {
  PixelSearch, FoundBeeX, FoundBeeY, 700, 113, 1080, 113, 0x873921, 0, Fast RGB
  if !ErrorLevel {
    FoundBeeY += 51
    loop, 10 {
      loop, 5 {
        ControlClick, % "x" FoundBeeX " y" FoundBeeY, %wintitle%,,,, NA
        Sleep, 40
      }
      FoundBeeX--
    }
  } else {
  }
}

level_active_heroes() {
  ScrollBarX = 555
  ScrollBarY = 276
  loop, 6 {
    ControlClick, % "x" ScrollBarX " y" ScrollBarY, %wintitle%,,,, NA
    ControlClick, x870 y510, %wintitle%,,,, NA
    Sleep, 350
    PixelSearch, LevelableX, LevelableY, 43, 204, 73, 669, 0xFFF90A, 0, Fast RGB
    toggle := true
    while (toggle) {
      if !ErrorLevel {
        Send {Q down}
        Sleep, 50
        ControlClick, % "x" LevelableX " y" LevelableY, %wintitle%,,,, NA
        Send {Q up}
        Sleep, 740
        PixelSearch, LevelableX, LevelableY, 43, 204, 73, 669, 0xFFF90A, 0, Fast RGB
        Sleep, 450
      } else {
        ScrollBarY += 61
        toggle := false
      }
    }
  }
}

ResizeWin(Width,Height) {
  WinGetPos,X,Y,W,H,%wintitle%
  if (W != %Width% && H != %Height%) {
    WinMove,%wintitle%,,%X%,%Y%,%Width%,%Height%
  }
}

scan_for_fish(GivenX, GivenY, fish_seek_color) {
  PixelSearch, ReceivedX, ReceivedY, %GivenX%, %GivenY%, %GivenX%, %GivenY%, %fish_seek_color%, 24, Fast RGB
  Sleep, 500
  if !ErrorLevel {
    ControlClick, x%GivenX% y%GivenY%, %wintitle%,,,, NA
  } else {
  }
}

unlock_prog_mode() {
  PixelGetColor, color, 1121, 279, RGB
  Sleep, 100
  if (color = 0xFF0000) {
    ControlClick, % "x" 1121 " y" 279, %wintitle%,,,, NA
  }
}

!u::
  ControlClick, x555 y640, %wintitle%,,,, NA
  Sleep, 350
  ControlClick, x450 y580, %wintitle%,,,, NA
  Sleep, 100
  ControlClick, x555 y600, %wintitle%,,,, NA
  return

F8::
  MouseGetPos, xpos, ypos
  Send {Z down}
  Sleep, 50
  ControlClick, % "x" xpos " y" ypos, %wintitle%,,,, NA
  Send {Z up}
  return

F9::
  ControlClick, % "x" 1121 " y" 279, %wintitle%,,,, NA
  return

F11::Pause
F12::ExitApp
