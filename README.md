# AHK scripts for Clicker Heroes

(note: I did not originally create these scripts. I found these on Steam and thought it would be nice to share them somewhere we could improve upon them)

These scripts are brought to you by [bpbrother](https://steamcommunity.com/id/bpbrother/myworkshopfiles/?section=guides&appid=363970)

## Before we get started...

Thought I'd share the scripts I made for this game.

- I expect you to know what AHK is, if you don't, google Autohotkey. It's a free program you need to download and install.
- Scripts made for **1136x640** resolution (or **1152x679** with the application header). The script will set this automatically for you. **Do not use on any other resolution**, they won't work correctly or at all.
- These scripts are non-intrusive. Meaning, you can work with your PC like normal, given you keep Clicker Heroes open in the background, some functionality will not be there if the game is minimized, not visible on screen, or behind other applications. Please refer to the scripts for details.
- I've defined keys for pausing and stopping a script, these are always to be found at the very bottom, you can freely adjust them to hotkeys you prefer.

Happy autoclicking!

## Active Clicking Script

A script that clicks for you and collects orange-fishy-clickables.

Additionally, it will also auto-MAX-level all uncollapsed heroes in your hero list (you can minimize heroes by either holding down '_Control_', '_Shift_' or '_Z_' and click the minus symbol next to the hero portrait, it will then collapse and the buy option will no longer be there, preventing misbuys), catch the bee for additional spell buffs and deselect farm mode (if set) after leveling heroes.

**Please note**: getting the bee, auto-leveling-heroes and unlocking farm mode will not work, when the game window is not active (you can make it so by clicking somewhere within Clicker Heroes). Click for damage and catching ruby fishes however will always work, even if the game is not visible on your screen.

Do not minimize Clicker Heroes, the script will work but the game will take a very long time to come back up and it can crash whilst trying to do so.

Additional features:

- switch farm / progress mode by hotkey F9 (can be changed)
- upgrade selected hero by x25 levels by hotkey F8 (can be changed)

Double Dark Ritual will be cast as well and other abilities when not conflicting with Dark Ritual. **Please note**: it will take one Clickstorm interval (2min30 at 75%, 10min at 0%) before the script starts using abilties!

## Idle Script (Experimental Build)

Early build of a working script, it casts DDR for you, levels your heroes every now and then, catches bees and fishies... more features in the works.

## FAQs

### How do I use these scripts?

Copy the code in a text document and save as an .AHK file with a name of your choice, run the game, then double-click your AHK file and watch the magic.

### Is there anything I should do / not do while the script is running?

You can do anything you want, play another game, watch streams, whatever... just don't minimize Clicker Heroes.

### What about the idle script?

That one is still flat as I haven't spent much time on playing the game 'idle'. Additions to be made...

### Are you a programer?

No, frankly I suck at it. Most people seeing the scripts can do better but they work for me and that is what counts. If you're like me but too lazy to get into AHK, these scripts are for you. I've played around and came up with the stuff I have now to make my CH gaming easier.

### Is that all?

Pretty much, I haven't changed these in a while, they work great, i might add clan-raiding, or auto-hero-leveling some day as it does get a bit tedious to level them again and again after ascension. I will not update this guide too often but if there's a huge change or new stuff, I will.
